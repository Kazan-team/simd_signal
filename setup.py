# SPDX-License-Identifier: LGPL-3-or-later
# See Notices.txt for copyright information
from setuptools import setup, find_packages
version = '0.0.1'

install_requires = [
    'nmigen',
]

setup(
    name='libresoc-simd-signal',
    version=version,
    description="Proof-of-concept Signal with dynamically-partitioned "
                "SIMD semantics and arbitrarily-adjustable lane bit-widths",
    classifiers=[
        "Topic :: Software Development",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    keywords='nmigen libre-soc simd',
    author='Jacob Lifshay',
    author_email='programmerjake@gmail.com',
    url='',
    license='LGPLv3+',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires
)
