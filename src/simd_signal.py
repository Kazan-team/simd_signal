# SPDX-License-Identifier: LGPL-3-or-later
# See Notices.txt for copyright information
from abc import ABCMeta, abstractmethod
import enum
from typing import (Any, Callable, ContextManager, Dict, Iterable, List, Mapping, Optional,
                    Sequence, Tuple, Type, TypeVar, Union, final, overload)
from contextlib import contextmanager
from enum import Enum
import threading
import typing
import nmigen
from nmigen.hdl.ast import (Assign, Const, Shape, Signal, Value, ValueCastable,
                            unsigned)
from nmigen.hdl.dsl import Module
from warnings import warn
from nmigen.hdl.ir import Elaboratable

_T = TypeVar('_T')
_T2 = TypeVar('_T2')

_ValueCastableType = Union[Value, int, Enum, ValueCastable]
_ValueCastableSequence = Sequence[_ValueCastableType]

_Log2PartCountIn = Union[None, int, "SimdPartMode"]
"""input type for `SimdPartMode.get_log2_part_count`"""

_LaneItemsInput = Union[Mapping[int, _T], Callable[[int], _T], _T]
"""input type for `SimdPartMode.get_lane_items`"""

_LaneValuesCastable = _LaneItemsInput[_ValueCastableType]
_SimdValueCastable = Union["SimdValue", _LaneValuesCastable]

_ValuePattern = Union[int, str, Enum]
"""input type for `Value.match`"""


def _cast_to_value_pattern(pattern: _ValuePattern) -> _ValuePattern:
    if not isinstance(pattern, (int, str, Enum)):
        raise TypeError("pattern must be an int, str, or Enum")
    return pattern


_SimdValuePatternCastable = _LaneItemsInput[_ValuePattern]
"""input type for `SimdValue.match`"""


class SupportedDomain(Enum):
    Comb = "comb"
    Sync = "sync"


class SimdSyntaxError(Exception):
    pass


class _StatementContext(Enum):
    TopLevel = enum.auto()
    Conditional = enum.auto()
    Switch = enum.auto()

    def assert_not_switch(self, statement: "SimdStatement") -> None:
        if self is _StatementContext.Switch:
            raise SimdSyntaxError(
                f"{type(statement).__name__} is not allowed inside a Switch "
                f"block, are you missing an intervening Case block?")

    def assert_is_switch(self, statement: "SimdStatement") -> None:
        if self is not _StatementContext.Switch:
            raise SimdSyntaxError(
                f"{type(statement).__name__} is only allowed inside a Switch "
                f"block")

    def assert_is_top_level(self, statement: "SimdStatement") -> None:
        if self is not _StatementContext.TopLevel:
            raise SimdSyntaxError(
                f"{type(statement).__name__} is only allowed outside any "
                f"other statement blocks")


class SimdStatement(metaclass=ABCMeta):
    class __NotInModule:
        pass
    __parent: Union[None, "SimdStatement", __NotInModule] = __NotInModule()
    domain: Optional[SupportedDomain] = None

    @property
    def parent(self) -> Optional["SimdStatement"]:
        if isinstance(self.__parent, SimdStatement.__NotInModule):
            return None
        return self.__parent

    @abstractmethod
    def needed_submodules(self) -> Iterable[Elaboratable]:
        raise NotImplementedError()

    @abstractmethod
    def all_statements(self) -> Iterable["SimdStatement"]:
        return (self,)

    @abstractmethod
    def __repr__(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def _child_context(self) -> _StatementContext:
        raise NotImplementedError()

    @abstractmethod
    def _check_parent_context(self, parent_context: _StatementContext) -> None:
        parent_context.assert_not_switch(self)

    def _on_module_insertion(self,
                             parent: Optional["SimdStatement"],
                             domain: Optional[SupportedDomain]):
        assert isinstance(self.__parent, SimdStatement.__NotInModule)
        self.__parent = parent
        self.domain = domain
        parent_context = _StatementContext.TopLevel
        if parent is not None:
            parent_context = parent._child_context()
        self._check_parent_context(parent_context)


class SimdConditionalStatement(SimdStatement):
    @final
    def _child_context(self) -> _StatementContext:
        return _StatementContext.Conditional


class SimdSimpleStatement(SimdStatement):
    """SimdStatement that doesn't contain any statements"""

    @final
    def _child_context(self) -> _StatementContext:
        raise SimdSyntaxError(f"can't nest statements inside"
                              f" {type(self).__name__}")

    @final
    def all_statements(self) -> Iterable["SimdStatement"]:
        return super().all_statements()


@final
class SimdIfStatement(SimdConditionalStatement):
    def __init__(self, condition: _SimdValueCastable,
                 then_statements: Iterable[SimdStatement] = (),
                 else_statements: Optional[Iterable[SimdStatement]] = None, *,
                 part_mode: Optional["SimdPartMode"] = None,
                 src_loc_at: int = 0):
        super().__init__()
        self.condition = SimdValue.cast(condition, part_mode=part_mode,
                                        src_loc_at=1 + src_loc_at).bool()
        self.then_statements = list(then_statements)
        if else_statements is not None:
            else_statements = list(else_statements)
        self.else_statements = else_statements

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from self.condition.needed_submodules()
        for i in self.then_statements:
            yield from i.needed_submodules()
        if self.else_statements is not None:
            for i in self.else_statements:
                yield from i.needed_submodules()

    def all_statements(self) -> Iterable[SimdStatement]:
        yield from super().all_statements()
        for i in self.then_statements:
            yield from i.all_statements()
        if self.else_statements is not None:
            for i in self.else_statements:
                yield from i.all_statements()

    def _check_parent_context(self, parent_context: _StatementContext) -> None:
        super()._check_parent_context(parent_context)

    def __repr__(self) -> str:
        if self.else_statements is None:
            return (f"SimdIfStatement({self.condition}, "
                    f"{self.then_statements})")
        return (f"SimdIfStatement({self.condition}, "
                f"{self.then_statements}, {self.else_statements})")


@final
class SimdCase(SimdConditionalStatement):
    """a Case in a Switch.
    if there are no patterns, then this is the Default"""

    def __init__(self, *patterns: _SimdValuePatternCastable,
                 statements: Iterable[SimdStatement] = (),
                 part_mode: Optional["SimdPartMode"] = None,
                 src_loc_at: int = 0):
        super().__init__()
        part_mode = SimdPartMode.get(part_mode)
        self.patterns = SimdValue.get_lane_patterns(
            *patterns, part_mode=part_mode, src_loc_at=1 + src_loc_at)
        self.statements = list(statements)

    def all_statements(self) -> Iterable[SimdStatement]:
        yield from super().all_statements()
        for i in self.statements:
            yield from i.all_statements()

    def __repr__(self) -> str:
        patterns = ', '.join(repr(i) for i in self.patterns)
        if len(self.patterns) > 0:
            patterns += ', '
        return f"SimdCase({patterns}statements={self.statements})"

    def _check_parent_context(self, parent_context: _StatementContext) -> None:
        parent_context.assert_is_switch(self)

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from super().needed_submodules()
        for i in self.statements:
            yield from i.needed_submodules()


@final
class SimdCases(List[SimdCase]):
    pass


@final
class SimdSwitch(SimdStatement):
    def __init__(self, value: _SimdValueCastable,
                 cases: Iterable[SimdCase] = (), *,
                 part_mode: Optional["SimdPartMode"] = None,
                 src_loc_at: int = 0):
        super().__init__()
        self.value = SimdValue.cast(value, part_mode=part_mode,
                                    src_loc_at=1 + src_loc_at)
        self.cases = SimdCases(cases)

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from self.value.needed_submodules()
        for case in self.cases:
            yield from case.needed_submodules()

    def all_statements(self) -> Iterable["SimdStatement"]:
        yield from super().all_statements()
        for case in self.cases:
            yield from case.all_statements()

    def __repr__(self) -> str:
        return f"SimdSwitch({self.value}, {self.cases})"

    def _child_context(self) -> _StatementContext:
        return _StatementContext.Switch

    def _check_parent_context(self, parent_context: _StatementContext) -> None:
        parent_context.assert_not_switch(self)


_SimdStatementsIn = Union[Iterable[Iterable[SimdStatement]],
                          Iterable[SimdStatement],
                          SimdStatement]


def _get_statements(statements: _SimdStatementsIn) -> Iterable[SimdStatement]:
    if isinstance(statements, Iterable):
        for s in statements:
            yield from _get_statements(s)
    else:
        yield statements


@final
class _SimdModuleDomain:
    def __init__(self, module: "SimdModule", domain: SupportedDomain):
        self.__module = module
        self.__domain = domain

    def __iadd__(self, statements: _SimdStatementsIn) -> "_SimdModuleDomain":
        self.__module._add_statements(statements, domain=self.__domain)
        return self


@final
class _SimdModuleDomains:
    __module: "SimdModule"
    comb: _SimdModuleDomain
    sync: _SimdModuleDomain

    def __init__(self, module: "SimdModule"):
        object.__setattr__(self, '__module', module)

    def __setattr__(self, name: str, value: Any) -> None:
        self.__setitem__(name, value)

    def __getattr__(self, name: str) -> _SimdModuleDomain:
        retval = _SimdModuleDomain(self.__module, SupportedDomain(name))
        object.__setattr__(self, name, retval)
        return retval

    def __setitem__(self, name: Any, value: Any) -> None:
        raise NotImplementedError("adding new domains is not supported")

    def __getitem__(self, name: Any) -> _SimdModuleDomain:
        if not isinstance(name, str):
            raise TypeError("clock domain names must be strings")
        return getattr(self, name)


@final
class SimdModule(Elaboratable):
    __top_level_statements: List[SimdStatement]
    __current_statements: Union[List[SimdStatement], SimdCases]
    __parent_statement: Optional[SimdStatement]

    def __init__(self, part_mode: Optional["SimdPartMode"] = None):
        self.d = _SimdModuleDomains(self)
        self.__top_level_statements = []
        self.__current_statements = self.__top_level_statements
        self.__parent_statement = None
        self.__part_mode = SimdPartMode.get(part_mode)

    def elaborate(self, platform) -> Module:
        m = Module()
        if self.__parent_statement is not None:
            raise ValueError("can't elaborate SimdModule while "
                             "nested inside statements")
        raise NotImplementedError()
        return m

    @contextmanager
    def __statement_scope(self, parent: Optional[SimdStatement],
                          target: Union[List[SimdStatement], SimdCases]):
        old_parent = self.__parent_statement
        old_target = self.__current_statements
        self.__current_statements = target
        self.__parent_statement = parent
        try:
            yield
        finally:
            self.__parent_statement = old_parent
            self.__current_statements = old_target

    def __add_statements_to(
        self,
        target: Union[List[SimdStatement], SimdCases],
        statements: _SimdStatementsIn, *,
        domain: Union[None, SupportedDomain, str] = None
    ):
        if domain is not None:
            domain = SupportedDomain(domain)
        for s in _get_statements(statements):
            s._on_module_insertion(self.__parent_statement, domain)
            if isinstance(target, SimdCases):
                assert isinstance(s, SimdCase)
                target.append(s)
            else:
                target.append(s)

    def _add_statements(self,
                        statements: _SimdStatementsIn, *,
                        domain: Union[None, SupportedDomain, str] = None):
        self.__add_statements_to(self.__current_statements, statements,
                                 domain=domain)

    def If(self, condition: _SimdValueCastable, *, src_loc_at: int = 0) -> ContextManager[None]:
        if_s = SimdIfStatement(condition, part_mode=self.__part_mode,
                               src_loc_at=1 + src_loc_at)
        self._add_statements(if_s)
        return self.__statement_scope(if_s, if_s.then_statements)

    def __else_helper(self, name: str) -> Tuple[SimdIfStatement,
                                                List[SimdStatement]]:
        if_s = None
        try:
            if_s = self.__current_statements[-1]
        except IndexError:
            pass
        if not isinstance(if_s, SimdIfStatement) \
                or if_s.else_statements is not None:
            raise SimdSyntaxError("{name} must follow If or Elif")
        if_s.else_statements = []
        return if_s, if_s.else_statements

    def Else(self) -> ContextManager[None]:
        if_s, else_statements = self.__else_helper("Else")
        return self.__statement_scope(if_s, else_statements)

    def Elif(self, condition: _SimdValueCastable, *,
             src_loc_at: int = 0) -> ContextManager[None]:
        outer_if, else_statements = self.__else_helper("Elif")
        inner_if = SimdIfStatement(condition, part_mode=self.__part_mode,
                                   src_loc_at=1 + src_loc_at)
        self.__add_statements_to(else_statements, inner_if)
        return self.__statement_scope(inner_if, inner_if.then_statements)

    def Switch(self, value: _SimdValueCastable, *,
               src_loc_at: int = 0) -> ContextManager[None]:
        switch = SimdSwitch(value, part_mode=self.__part_mode,
                            src_loc_at=1 + src_loc_at)
        self._add_statements(switch)
        return self.__statement_scope(switch, switch.cases)

    def Case(self, *patterns: _SimdValuePatternCastable,
             src_loc_at: int = 0) -> ContextManager[None]:
        case = SimdCase(*patterns, part_mode=self.__part_mode,
                        src_loc_at=1 + src_loc_at)
        self._add_statements(case)
        return self.__statement_scope(case, case.statements)

    def Default(self, *, src_loc_at: int = 0) -> ContextManager[None]:
        return self.Case(src_loc_at=1 + src_loc_at)


@final
class SimdLane:
    def __init__(self, start: int, size: int,
                 log2_part_count: _Log2PartCountIn = None):
        self.log2_part_count = SimdPartMode.get_log2_part_count(
            log2_part_count)
        assert size != 0
        size_is_pow2 = (size & (size - 1)) == 0
        assert size_is_pow2
        assert start >= 0
        assert start + size <= self.part_count
        assert start % size == 0
        self.start = start
        self.size = size

    @property
    def part_count(self) -> int:
        return 1 << self.log2_part_count

    @staticmethod
    def for_part(part_index: int, size: int,
                 log2_part_count: _Log2PartCountIn = None) -> "SimdLane":
        """get the lane that contains the part at `part_index`"""
        assert size != 0
        return SimdLane((part_index // size) * size, size, log2_part_count)

    def __repr__(self) -> str:
        return (f"SimdLane(start={self.start}, size={self.size}, "
                f"log2_part_count={self.log2_part_count}, "
                f"part_count={self.part_count})")

    @overload
    def is_active(self, part_starts: Sequence[bool]) -> bool: ...

    @overload
    def is_active(self, part_starts: _ValueCastableSequence) -> Value: ...

    def is_active(self, part_starts):
        assert len(part_starts) == 1 + self.part_count
        first = part_starts[self.start]
        if not isinstance(first, bool):
            first = Value.cast(first)
        last = part_starts[self.start + self.size]
        if not isinstance(last, bool):
            last = Value.cast(last)
        retval = first & last
        for i in range(self.start + 1, self.start + self.size):
            part_start = part_starts[i]
            if isinstance(part_start, bool):
                retval &= not part_start
            else:
                retval &= ~Value.cast(part_start)
        return retval

    def __eq__(self, o: object) -> bool:
        if isinstance(o, SimdLane):
            return (self.log2_part_count == o.log2_part_count
                    and self.start == o.start
                    and self.size == o.size)
        return NotImplemented

    def __hash__(self) -> int:
        return hash((self.log2_part_count, self.start, self.size))

    @staticmethod
    def lane_sizes(log2_part_count: _Log2PartCountIn = None) -> Iterable[int]:
        log2_part_count = SimdPartMode.get_log2_part_count(log2_part_count)
        for i in range(1 + log2_part_count):
            yield 1 << i

    @staticmethod
    def lane_starts(lane_size: int,
                    log2_part_count: _Log2PartCountIn = None) -> Iterable[int]:
        log2_part_count = SimdPartMode.get_log2_part_count(log2_part_count)
        part_count = 1 << log2_part_count
        assert 0 < lane_size <= part_count
        return range(0, part_count, lane_size)

    @staticmethod
    def lanes_with_size(lane_size: int,
                        log2_part_count: _Log2PartCountIn = None,
                        ) -> Iterable["SimdLane"]:
        log2_part_count = SimdPartMode.get_log2_part_count(log2_part_count)
        for lane_start in SimdLane.lane_starts(lane_size, log2_part_count):
            yield SimdLane(lane_start, lane_size, log2_part_count)

    @staticmethod
    def lanes(log2_part_count: _Log2PartCountIn = None
              ) -> Iterable["SimdLane"]:
        log2_part_count = SimdPartMode.get_log2_part_count(log2_part_count)
        for size in SimdLane.lane_sizes(log2_part_count):
            yield from SimdLane.lanes_with_size(size, log2_part_count)


@final
class SimdPartMode:
    """Signals describing how all SimdSignals are partitioned into logical lanes.
    if `self.part_starts[i]` is True, then the partition extending from part #i
    to the next set `part_starts` is logically a single lane.
    all logical lanes must be power-of-two sizes and be aligned to their size;
    this means that SimdSignals are always split up in the same way a buddy
    memory allocator works.
    """

    part_starts: List[Union[Const, Signal]]

    __current = threading.local()

    @classmethod
    def get_opt(cls, part_mode: Optional["SimdPartMode"] = None) -> Optional["SimdPartMode"]:
        if part_mode is not None:
            return part_mode
        return getattr(cls.__current, "current", None)

    @classmethod
    def get(cls, part_mode: Optional["SimdPartMode"] = None) -> "SimdPartMode":
        retval = cls.get_opt(part_mode)
        if retval is None:
            raise ValueError("Not in a SimdPartMode.scope")
        return retval

    @classmethod
    @contextmanager
    def scope(cls, part_mode: "SimdPartMode"):
        assert isinstance(part_mode, SimdPartMode)
        old = cls.get_opt()
        cls.__current.current = part_mode
        try:
            yield
        finally:
            cls.__current.current = old

    def __init__(self, log2_part_count: _Log2PartCountIn = None):
        self.log2_part_count = self.get_log2_part_count(log2_part_count)
        self.part_starts = [Const(1)] * (self.part_count + 1)
        for i in range(1, self.part_count):
            self.part_starts[i] = Signal(name=f"part_starts_{i}")

    @staticmethod
    def get_log2_part_count(log2_part_count: _Log2PartCountIn = None) -> int:
        if not isinstance(log2_part_count, int):
            log2_part_count = SimdPartMode.get(log2_part_count).log2_part_count
        assert log2_part_count >= 0
        return log2_part_count

    @property
    def part_count(self) -> int:
        return 1 << self.log2_part_count

    def lane_sizes(self) -> Iterable[int]:
        return SimdLane.lane_sizes(self)

    def lane_starts(self, lane_size: int) -> Iterable[int]:
        return SimdLane.lane_starts(lane_size, self)

    def lanes_with_size(self, lane_size: int) -> Iterable[SimdLane]:
        return SimdLane.lanes_with_size(lane_size, self)

    def lanes(self) -> Iterable[SimdLane]:
        return SimdLane.lanes(self)

    def is_lane_active(self, lane: SimdLane) -> Value:
        assert lane.log2_part_count == self.log2_part_count
        return lane.is_active(self.part_starts)

    @overload
    @staticmethod
    def __is_valid_helper(part_starts: Sequence[bool],
                          full_log2_part_count: int,
                          start: int,
                          current_log2_part_count: int) -> bool: ...

    @overload
    @staticmethod
    def __is_valid_helper(part_starts: _ValueCastableSequence,
                          full_log2_part_count: int,
                          start: int,
                          current_log2_part_count: int) -> Value: ...

    @staticmethod
    def __is_valid_helper(part_starts,
                          full_log2_part_count,
                          start,
                          current_log2_part_count):
        """checks that the region in `start` through
        `start + 2 ** current_log2_part_count` are split into valid parts of
        size at most `2 ** current_log2_part_count` parts"""
        # valid if it's either a full-width lane
        retval = SimdLane(start, 1 << current_log2_part_count,
                          full_log2_part_count).is_active(part_starts)
        if current_log2_part_count > 0:
            next_log2_count = current_log2_part_count - 1
            last_half_start = start + (1 << next_log2_count)
            # or each half is valid
            first_half = SimdPartMode.__is_valid_helper(part_starts,
                                                        full_log2_part_count,
                                                        start,
                                                        next_log2_count)
            last_half = SimdPartMode.__is_valid_helper(part_starts,
                                                       full_log2_part_count,
                                                       last_half_start,
                                                       next_log2_count)
            retval |= first_half & last_half
        return retval

    @overload
    @staticmethod
    def are_part_starts_valid(
        part_starts: Sequence[bool],
        log2_part_count: _Log2PartCountIn = None
    ) -> bool: ...

    @overload
    @staticmethod
    def are_part_starts_valid(
        part_starts: Sequence[_ValueCastableType],
        log2_part_count: _Log2PartCountIn = None
    ) -> Value: ...

    @staticmethod
    def are_part_starts_valid(part_starts, log2_part_count=None):
        log2_part_count = SimdPartMode.get_log2_part_count(log2_part_count)
        assert len(part_starts) == 1 + (1 << log2_part_count)
        return SimdPartMode.__is_valid_helper(part_starts, log2_part_count,
                                              0, log2_part_count)

    def is_valid(self) -> Value:
        """check that all enabled parts are at valid locations/sizes"""
        return SimdPartMode.are_part_starts_valid(self.part_starts, self)

    def __repr__(self) -> str:
        return (f"SimdPartMode("
                f"log2_part_count={self.log2_part_count}, "
                f"part_count={self.part_count})")

    def __eq__(self, o: object) -> bool:
        return self is o

    def eq(self, rhs: "SimdPartMode") -> List[Assign]:
        assert self.part_count == rhs.part_count
        return [
            self.part_starts[i].eq(rhs.part_starts[i])
            for i in range(1, self.part_count)
        ]

    def get_lane_items(self, input_items: _LaneItemsInput[_T],
                       *,
                       cast: Callable[[_T], _T2],
                       filter_callable: Callable[[_LaneItemsInput[_T]], bool]
                       = lambda v: isinstance(v, Enum),
                       item_name: str = "item",
                       src_loc_at: int = 0) -> Dict[int, _T2]:
        """get a dict mapping lane-sizes to items from a
        user input (`input_items`), the user input can be:
        1. a mapping from lane-sizes to items.
        2. a callable that maps from lane-sizes to items.
        3. a single item.

        callable inputs are only recognized if `filter_callable(input_items)`
        returns False."""
        retval: Dict[int, _T2] = {}
        if isinstance(input_items, Mapping):
            for lane_size in self.lane_sizes():
                try:
                    item = input_items[lane_size]
                except KeyError as e:
                    raise ValueError(f"missing lane {item_name} "
                                     f"for lane_size={e.args[0]}") from e
                retval[lane_size] = cast(item)
            for k in input_items:
                if k not in retval:
                    warn(f"SimdLayout: "
                         f"unused lane {item_name}: {k, input_items[k]!r}",
                         UnusedLaneItemWarning,
                         stacklevel=2 + src_loc_at)
                    break
        elif callable(input_items) and not filter_callable(input_items):
            for lane_size in self.lane_sizes():
                retval[lane_size] = cast(input_items(lane_size))
        else:
            cast_item = cast(typing.cast(_T, input_items))
            for lane_size in self.lane_sizes():
                retval[lane_size] = cast_item
        return retval


class SimdPaddingKind(Enum):
    SignExtended = "sign-extended"
    """all padding bits are copies of the corresponding lane's MSB"""

    Zeros = "zeros"
    """all padding bits are zeros"""

    Ones = "ones"
    """all padding bits are ones"""

    Other = "other"
    """padding bits are none of the other options"""

    Repeating = "repeating"
    """padding bits are repeating copies of the corresponding lane -- as if
    produced by `Repl(lane_value, n)`"""

    NoPadding = "no-padding"
    """the layout has no padding bits"""

    def normalized(self, layout: "_SimdLayoutCastable", *,
                   part_mode: Optional[SimdPartMode] = None,
                   src_loc_at: int = 0) -> "SimdPaddingKind":
        """normalize the padding kind for the provided layout.
        doesn't modify self."""
        layout = SimdLayout.cast(layout, part_mode,
                                 src_loc_at=1 + src_loc_at)
        if not layout.has_padding:
            return SimdPaddingKind.NoPadding
        assert not layout.is_empty  # empty layouts never have padding
        if self is SimdPaddingKind.NoPadding:
            self = SimdPaddingKind.Other
        if layout.is_bool():
            if (self is SimdPaddingKind.SignExtended
                    or self is SimdPaddingKind.Zeros
                    or self is SimdPaddingKind.Ones
                    or self is SimdPaddingKind.Other):
                return self
            assert self is SimdPaddingKind.Repeating
            return SimdPaddingKind.SignExtended
        return self


class UnusedLaneItemWarning(UserWarning):
    pass


# types that can be passed to Shape.cast
_ShapeCastable = Union[int, Tuple[int, bool], Type[Enum], Shape]

# types that can be passed to SimdLayout
_SimdLayoutCastable = Union["SimdLayout",
                            _LaneItemsInput[_ShapeCastable]]


@final
class SimdLayout:
    part_mode: SimdPartMode

    lane_shapes: Dict[int, Shape]
    """mapping from the size in parts of a lane to the `Shape` of that lane"""

    padded_bits_per_part: int

    @staticmethod
    def cast(lane_shapes: _SimdLayoutCastable,
             part_mode: Optional[SimdPartMode] = None, *,
             src_loc_at: int = 0) -> "SimdLayout":
        if isinstance(lane_shapes, SimdLayout):
            return lane_shapes
        return SimdLayout(lane_shapes, part_mode, src_loc_at=1 + src_loc_at)

    def __init__(self,
                 lane_shapes: _SimdLayoutCastable,
                 part_mode: Optional[SimdPartMode] = None, *,
                 src_loc_at: int = 0):
        self.part_mode = SimdPartMode.get(part_mode)
        if isinstance(lane_shapes, SimdLayout):
            assert self.part_mode == lane_shapes.part_mode
            self.lane_shapes = lane_shapes.lane_shapes.copy()
        else:
            self.lane_shapes = self.part_mode.get_lane_items(
                lane_shapes, cast=Shape.cast,
                item_name="shape", src_loc_at=1 + src_loc_at)
        self.padded_bits_per_part = self.lane_shapes[1].width
        for lane_size, shape in self.lane_shapes.items():
            needed = (shape.width + lane_size - 1) // lane_size
            self.padded_bits_per_part = max(self.padded_bits_per_part, needed)
            if shape.signed != self.lane_shapes[1].signed:
                raise ValueError("all lane shapes must have same signedness")

    def with_padding(self) -> "SimdLayout":
        """get a layout that is this layout, but including this layout's
        padding."""
        if not self.has_padding:
            return self
        return SimdLayout(lambda lane_size:
                          unsigned(self.lane_bit_width(lane_size,
                                                       include_padding=True)),
                          self.part_mode)

    @property
    def has_padding(self) -> bool:
        for lane_size in self.part_mode.lane_sizes():
            padded = self.lane_bit_width(lane_size, include_padding=True)
            if self.lane_bit_width(lane_size) != padded:
                return True
        return False

    def is_bool(
        self,
        padding_kind: SimdPaddingKind = SimdPaddingKind.SignExtended
    ) -> bool:
        for lane_size in self.part_mode.lane_sizes():
            if self.lane_bit_width(lane_size) != 1:
                return False
        if padding_kind is SimdPaddingKind.SignExtended:
            # bypass normalized for default value to avoid infinite recursion
            return True
        return padding_kind.normalized(self) is SimdPaddingKind.SignExtended

    @property
    def is_empty(self) -> bool:
        return self.width == 0

    @property
    def width(self) -> int:
        return self.padded_bits_per_part * self.part_mode.part_count

    @property
    def lanes_signed(self) -> bool:
        return self.lane_shapes[1].signed

    def __repr__(self) -> str:
        return (f"SimdLayout("
                f"lane_shapes={self.lane_shapes!r}, "
                f"padded_bits_per_part={self.padded_bits_per_part!r}, "
                f"width={self.width!r}, "
                f"lanes_signed={self.lanes_signed!r}, "
                f"part_mode={self.part_mode!r})")

    def __eq__(self, o: object) -> bool:
        if isinstance(o, SimdLayout):
            return (self.part_mode == o.part_mode
                    and self.lane_shapes == o.lane_shapes)
        return NotImplemented

    def part_start_bit(self, part_index: int) -> int:
        return part_index * self.padded_bits_per_part

    def lane_start_bit(self, lane: SimdLane) -> int:
        assert self.part_mode.log2_part_count == lane.log2_part_count
        return self.part_start_bit(lane.start)

    def lane_bit_width(self, lane_size: Union[SimdLane, int], *, include_padding: bool = False) -> int:
        if isinstance(lane_size, SimdLane):
            assert self.part_mode.log2_part_count == lane_size.log2_part_count
            lane_size = lane_size.size
        if include_padding:
            return lane_size * self.padded_bits_per_part
        else:
            return self.lane_shapes[lane_size].width

    def lane_bits_slice(self, lane: SimdLane, *, include_padding: bool = False) -> slice:
        start_bit = self.lane_start_bit(lane)
        bit_width = self.lane_bit_width(lane, include_padding=include_padding)
        return slice(start_bit, start_bit + bit_width)

    def part_bits_slice(self, part_index: int) -> slice:
        start_bit = self.part_start_bit(part_index)
        return slice(start_bit, start_bit + self.padded_bits_per_part)

    def merge_padded_lanes_into_full_width_values(
        self, padded_lanes: Mapping[SimdLane, _ValueCastableType]
    ) -> Dict[int, Value]:
        full_width_values: Dict[int, Value] = {}
        for lane_size in self.part_mode.lane_sizes():
            values: List[Value] = []
            lane_shape = unsigned(self.lane_bit_width(lane_size,
                                                      include_padding=True))
            for lane in self.part_mode.lanes_with_size(lane_size):
                value = Value.cast(padded_lanes[lane])
                assert value.shape() == lane_shape
                values.append(value)
            full_width_values[lane_size] = nmigen.Cat(values)
        return full_width_values

    def merge_full_width_values(
        self, full_width_values: Mapping[int, _ValueCastableType]
    ) -> Value:
        """merge full-width values for each lane size into a single full-width
        `Value` suitable for assignment to `SimdValue.underlying`.
        `full_width_values` maps lane sizes to the nmigen `Value`s to merge.
        """
        parts: List[Value] = []
        for part_index in range(self.part_mode.part_count):
            part_slice = self.part_bits_slice(part_index)
            part = 0
            for lane_size in self.part_mode.lane_sizes():
                lane = SimdLane.for_part(part_index, lane_size, self.part_mode)
                full_width_value = Value.cast(full_width_values[lane_size])
                assert full_width_value.shape().width == self.width
                active = self.part_mode.is_lane_active(lane)
                part = nmigen.Mux(active, full_width_value[part_slice], part)
            parts.append(part)
        return nmigen.Cat(*parts)

    @staticmethod
    def common(*layouts: _SimdLayoutCastable,
               part_mode: Optional[SimdPartMode] = None,
               src_loc_at: int = 0) -> "SimdLayout":
        """returns the common layout of all input layouts,
        as defined for `Shape` by `nmigen.Mux(0, lhs, rhs)`."""
        assert len(layouts) != 0
        if part_mode is None:
            for layout in layouts:
                if isinstance(layout, SimdLayout):
                    part_mode = layout.part_mode
                    break
        lane_shapes = None
        for layout in layouts:
            layout = SimdLayout.cast(layout,
                                     part_mode,
                                     src_loc_at=1 + src_loc_at)
            assert layout.part_mode == part_mode
            if len(layouts) == 1:
                return layout
            if lane_shapes is None:
                lane_shapes = layout.lane_shapes.copy()
                continue
            for lane_size in part_mode.lane_sizes():
                lhs = lane_shapes[lane_size]
                rhs = layout.lane_shapes[lane_size]
                lane_shapes[lane_size] = nmigen.Mux(0, Const(0, lhs),
                                                    Const(0, rhs)).shape()
        return SimdLayout(lane_shapes, part_mode, src_loc_at=1 + src_loc_at)


class SimdValue(metaclass=ABCMeta):
    def __init__(self, layout: _SimdLayoutCastable,
                 padding_kind: SimdPaddingKind,
                 underlying: Value, *,
                 part_mode: Optional[SimdPartMode] = None,
                 src_loc_at: int = 0):
        self.underlying = Value.cast(underlying)
        self.layout = SimdLayout.cast(layout, part_mode,
                                      src_loc_at=1 + src_loc_at)
        assert self.underlying.shape() == unsigned(self.layout.width)
        self.padding_kind = padding_kind.normalized(self.layout)

    @staticmethod
    def splat(v: _ValueCastableType,
              part_mode: Optional[SimdPartMode] = None, *,
              src_loc_at: int = 0) -> "SimdValue":
        value = Value.cast(v)
        part_mode = SimdPartMode.get(part_mode)
        layout = SimdLayout(value.shape(), part_mode)
        assert layout.padded_bits_per_part == value.width
        underlying = nmigen.Repl(value.as_unsigned(), part_mode.part_count)
        return SimdValue(layout, SimdPaddingKind.Repeating, underlying)

    @staticmethod
    def get_lane_values(lane_values: _LaneValuesCastable,
                        part_mode: SimdPartMode, *,
                        src_loc_at: int = 0) -> Dict[int, Value]:
        return part_mode.get_lane_items(lane_values,
                                        cast=Value.cast,
                                        item_name="value",
                                        src_loc_at=1 + src_loc_at)

    @staticmethod
    def get_lane_patterns(
        *lane_patterns: _SimdValuePatternCastable,
        part_mode: SimdPartMode,
        src_loc_at: int = 0
    ) -> List[Dict[int, _ValuePattern]]:
        retval: List[Dict[int, _ValuePattern]] = []
        for i in lane_patterns:
            retval.append(part_mode.get_lane_items(
                typing.cast(Any, i), cast=_cast_to_value_pattern,
                item_name="pattern", src_loc_at=1 + src_loc_at))
        return retval

    @staticmethod
    def cast(lane_values: _SimdValueCastable,
             part_mode: Optional[SimdPartMode] = None, *,
             src_loc_at: int = 0) -> "SimdValue":
        if isinstance(lane_values, SimdValue):
            return lane_values
        part_mode = SimdPartMode.get(part_mode)
        lane_values = SimdValue.get_lane_values(lane_values,
                                                part_mode,
                                                src_loc_at=1 + src_loc_at)
        is_splat = True
        for v in lane_values.values():
            if v is not lane_values[1]:
                is_splat = False
                break
        if is_splat:
            return SimdValue.splat(lane_values[1], part_mode,
                                   src_loc_at=1 + src_loc_at)
        return SimdLaneSizeLookupValue(lane_values,
                                       part_mode,
                                       src_loc_at=1 + src_loc_at)

    @final
    def convert_layout(self, target_layout: _SimdLayoutCastable, *,
                       src_loc_at: int = 0) -> "SimdValue":
        target_layout = SimdLayout.cast(target_layout,
                                        part_mode=self.layout.part_mode,
                                        src_loc_at=1 + src_loc_at)
        assert self.layout.part_mode == target_layout.part_mode
        if self.layout == target_layout:
            return self
        return SimdConvertLayout(self, target_layout,
                                 src_loc_at=1 + src_loc_at)

    def needed_submodules(self) -> Iterable[Elaboratable]:
        return ()

    def __repr__(self) -> str:
        return f"SimdValue(layout={self.layout}, underlying={self.underlying})"

    @final
    def lane(self, lane: SimdLane, *, include_padding: bool = False) -> Value:
        retval = self.underlying[self.layout.lane_bits_slice(
            lane, include_padding=include_padding)]
        if not include_padding and self.layout.lanes_signed:
            return retval.as_signed()
        return retval.as_unsigned()

    @final
    def is_bool(self) -> bool:
        return self.layout.is_bool(self.padding_kind)

    @final
    def bool(self) -> "SimdValue":
        if self.is_bool():
            return self
        return SimdBoolCast(self)

    @final
    def __bool__(self):
        raise TypeError("SimdValue can't ever be converted to a Python bool")


class SimdValueLaneSizewise(SimdValue, Elaboratable):
    """SimdValue that operates on a full-width value for each lane-size
    individually, then merges them all to create the final output."""

    full_width_values: Dict[int, Signal]

    def __init__(self, layout: _SimdLayoutCastable,
                 padding_kind: SimdPaddingKind,
                 part_mode: Optional[SimdPartMode] = None, *,
                 src_loc_at: int):
        layout = SimdLayout.cast(layout,
                                 part_mode,
                                 src_loc_at=1 + src_loc_at)
        assert part_mode == layout.part_mode or part_mode is None
        part_mode = layout.part_mode
        underlying = Signal(layout.width)
        super().__init__(layout, padding_kind, underlying)
        self.full_width_values = {}
        for lane_size in part_mode.lane_sizes():
            self.full_width_values[lane_size] = Signal(
                layout.width, name=f"full_width_value_{lane_size}")

    @abstractmethod
    def _get_full_width_value(self, lane_size: int) -> _ValueCastableType:
        raise NotImplementedError()

    @abstractmethod
    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield self

    def elaborate(self, platform) -> Module:
        m = Module()
        for lane_size, lhs in self.full_width_values.items():
            rhs = Value.cast(self._get_full_width_value(lane_size))
            assert rhs.shape() == unsigned(self.layout.width)
            m.d.comb += lhs.eq(self._get_full_width_value(lane_size))
        merged = self.layout.merge_full_width_values(self.full_width_values)
        m.d.comb += self.underlying.eq(merged)
        return m

    @abstractmethod
    def __repr__(self) -> str:
        return super().__repr__()


class SimdValueLanewise(SimdValueLaneSizewise, Elaboratable):
    """SimdValue that operates on each lane individually, then merges them
    all to create the final output"""

    padded_lane_values: Dict[SimdLane, Signal]

    def __init__(self, layout: _SimdLayoutCastable,
                 padding_kind: SimdPaddingKind,
                 part_mode: Optional[SimdPartMode] = None, *,
                 src_loc_at: int):
        super().__init__(layout,
                         padding_kind,
                         part_mode,
                         src_loc_at=1 + src_loc_at)
        self.padded_lane_values = {}
        for lane in self.layout.part_mode.lanes():
            w = self.layout.lane_bit_width(lane, include_padding=True)
            self.padded_lane_values[lane] = Signal(
                unsigned(w),
                name=f"padded_lane_value_{lane.start}_{lane.size}")
        self.__rhs_full_width_values = self.layout\
            .merge_padded_lanes_into_full_width_values(self.padded_lane_values)

    @final
    def _get_full_width_value(self, lane_size: int) -> _ValueCastableType:
        return self.__rhs_full_width_values[lane_size]

    @abstractmethod
    def _get_lane_value(self, lane: SimdLane) -> _ValueCastableType:
        raise NotImplementedError()

    def elaborate(self, platform) -> Module:
        m = super().elaborate(platform)
        for lane, padded_lane_value in self.padded_lane_values.items():
            m.d.comb += padded_lane_value.eq(self._get_lane_value(lane))
        return m


@final
class SimdConvertLayout(SimdValueLanewise, Elaboratable):
    """SimdValue that converts layouts: truncating, sign-extending, or
    zero-extending as needed. It can also fill padding with known bits.
    `self.layout` is the layout that is converted to.

    When converting, the source's signedness is used to decide sign-extension
    vs. zero-extension, since that retains the numerical values reduced modulo
    the destination `Shape`. This is the same behavior that nmigen, C, Rust,
    and most other programming languages use."""

    converted_lanes: Dict[SimdLane, Signal]
    padding_fill_value: Optional[Value]

    def __init__(self, input: _SimdValueCastable,
                 layout: _SimdLayoutCastable,
                 part_mode: Optional[SimdPartMode] = None, *,
                 padding_fill_value: Union[None, SimdPaddingKind,
                                           _ValueCastableType] = None,
                 src_loc_at: int = 0):
        self.input = SimdValue.cast(input, part_mode,
                                    src_loc_at=1 + src_loc_at)
        if padding_fill_value is None:
            if self.input.layout.lanes_signed:
                padding_kind = SimdPaddingKind.SignExtended
            else:
                padding_kind = SimdPaddingKind.Zeros
            self.padding_fill_value = None
        elif isinstance(padding_fill_value, SimdPaddingKind):
            padding_kind = padding_fill_value
            if (padding_kind is SimdPaddingKind.Other
                    or padding_kind is SimdPaddingKind.NoPadding):
                padding_kind = SimdPaddingKind.Zeros
            self.padding_fill_value = None
        else:
            self.padding_fill_value = Value.cast(padding_fill_value)
            assert self.padding_fill_value.shape() == unsigned(1)
            if isinstance(self.padding_fill_value, Const):
                if self.padding_fill_value.value != 0:
                    padding_kind = SimdPaddingKind.Ones
                else:
                    padding_kind = SimdPaddingKind.Zeros
            else:
                padding_kind = SimdPaddingKind.Other
        part_mode = self.input.layout.part_mode
        super().__init__(layout, padding_kind, part_mode,
                         src_loc_at=1 + src_loc_at)
        assert self.padding_kind is not SimdPaddingKind.Other, \
            "should be unreachable"
        if self.padding_kind is SimdPaddingKind.Ones:
            self.padding_fill_value = Const(1, 1)
        elif (self.padding_kind is SimdPaddingKind.Zeros
                or padding_kind is SimdPaddingKind.NoPadding):
            self.padding_fill_value = Const(0, 1)
        self.converted_lanes = {}
        for lane in part_mode.lanes():
            self.converted_lanes[lane] = Signal(
                unsigned(self.layout.lane_bit_width(lane)),
                f"converted_lane_{lane.start}_{lane.size}")

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from self.input.needed_submodules()
        yield from super().needed_submodules()

    def _get_lane_value(self, lane: SimdLane) -> Value:
        unpadded_width = self.layout.lane_bit_width(lane)
        padded_width = self.layout.lane_bit_width(lane,
                                                  include_padding=True)
        assert padded_width >= unpadded_width, "inconsistent layout"
        if self.padding_fill_value is not None:
            pad = nmigen.Repl(self.padding_fill_bit,
                              padded_width - unpadded_width)
        else:
            if self.padding_kind is SimdPaddingKind.SignExtended:
                pad = nmigen.Repl(self.converted_lanes[lane][-1],
                                  padded_width - unpadded_width)
            elif self.padding_kind is SimdPaddingKind.Zeros:
                assert False, "should be unreachable -- " \
                    "padding_fill_value should be set by __init__"
            elif self.padding_kind is SimdPaddingKind.Ones:
                assert False, "should be unreachable -- " \
                    "padding_fill_value should be set by __init__"
            elif self.padding_kind is SimdPaddingKind.Other:
                assert False, "should be unreachable -- caught by __init__"
            elif self.padding_kind is SimdPaddingKind.Repeating:
                repl_count = (padded_width
                              + unpadded_width - 1) // unpadded_width
                return nmigen.Repl(self.converted_lanes[lane],
                                   repl_count)[0:padded_width]
            else:
                assert self.padding_kind is SimdPaddingKind.NoPadding
                assert False, "should be unreachable -- " \
                    "padding_fill_value should be set by __init__"

        return nmigen.Cat(self.converted_lanes[lane], pad)

    def elaborate(self, platform) -> Module:
        m = super().elaborate(platform)
        for lane, converted_lane in self.converted_lanes.items():
            m.d.comb += converted_lane.eq(self.input.lane(lane))
        return m

    def __repr__(self) -> str:
        padding = self.padding_kind
        if self.padding_fill_value is not None:
            padding = self.padding_fill_value
        return (f"SimdConvertLayout(input={self.input}, layout={self.layout}, "
                f"padding_fill_value={padding})")


@final
class SimdLaneSizeLookupValue(SimdValueLanewise, Elaboratable):
    """SimdValue where each lane is set to a nmigen Value that is looked-up
    based on the lane's size in parts"""

    lane_inputs: Dict[int, Signal]

    def __init__(self, lane_values: _LaneValuesCastable,
                 part_mode: Optional[SimdPartMode] = None, *,
                 src_loc_at: int = 0):
        part_mode = SimdPartMode.get(part_mode)
        self.lane_values = SimdValue.get_lane_values(lane_values,
                                                     part_mode,
                                                     src_loc_at=1 + src_loc_at)
        layout = SimdLayout(lambda lane_size:
                            self.lane_values[lane_size].shape(),
                            part_mode)
        self.lane_inputs = {}
        for lane_size in self.layout.part_mode.lane_sizes():
            lane_width = self.layout.lane_bit_width(lane_size)
            lane_input = Signal(unsigned(lane_width),
                                name=f"lane_input_{lane_size}")
            self.lane_inputs[lane_size] = lane_input
        super().__init__(layout, SimdPaddingKind.Zeros, part_mode,
                         src_loc_at=1 + src_loc_at)

    def _get_lane_value(self, lane: SimdLane) -> Value:
        return self.lane_inputs[lane.size]

    def needed_submodules(self) -> Iterable[Elaboratable]:
        return super().needed_submodules()

    def elaborate(self, platform) -> Module:
        m = super().elaborate(platform)
        for lane_size in self.layout.part_mode.lane_sizes():
            lane_value = self.lane_values[lane_size]
            m.d.comb += self.lane_inputs[lane_size].eq(lane_value)
        return m

    def __repr__(self) -> str:
        return (f"SimdLaneSizeLookupValue(lane_values={self.lane_values}, "
                f"layout={self.layout})")


@final
class SimdCat(SimdValueLanewise):
    def __init__(
        self,
        *inputs: _SimdValueCastable,
        part_mode: Optional[SimdPartMode] = None,
        src_loc_at: int = 0,
    ):
        self.inputs = tuple(SimdValue.cast(i, part_mode=part_mode,
                                           src_loc_at=1 + src_loc_at)
                            for i in inputs)
        if len(self.inputs) > 0:
            assert part_mode is None \
                or part_mode == self.inputs[0].layout.part_mode
            part_mode = self.inputs[0].layout.part_mode
        else:
            part_mode = SimdPartMode.get(part_mode)
        for inp in self.inputs:
            assert part_mode == inp.layout.part_mode

        def calc_lane_shape(lane_size: int) -> Shape:
            retval = 0
            for inp in self.inputs:
                retval += inp.layout.lane_bit_width(lane_size)
            return unsigned(retval)
        layout = SimdLayout(calc_lane_shape, part_mode,
                            src_loc_at=1 + src_loc_at)
        super().__init__(layout, SimdPaddingKind.Zeros,
                         part_mode=part_mode,
                         src_loc_at=1 + src_loc_at)

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from super().needed_submodules()
        for i in self.inputs:
            yield from i.needed_submodules()

    def _get_lane_value(self, lane: SimdLane) -> _ValueCastableType:
        return nmigen.Cat(i.lane(lane) for i in self.inputs)

    def __repr__(self) -> str:
        return f"SimdCat({', '.join(repr(i) for i in self.inputs)})"


@final
class SimdRepl(SimdValueLanewise):
    def __init__(
        self,
        input: _SimdValueCastable,
        lane_repl_counts: _LaneItemsInput[int], *,
        part_mode: Optional[SimdPartMode] = None,
        src_loc_at: int = 0,
    ):
        self.input = SimdValue.cast(input, part_mode=part_mode,
                                    src_loc_at=1 + src_loc_at)
        assert part_mode is None or part_mode == self.input.layout.part_mode
        part_mode = self.input.layout.part_mode

        def check_lane_count(count: int) -> int:
            assert isinstance(count, int)
            assert count >= 0
            return count
        self.lane_repl_counts = part_mode.get_lane_items(
            lane_repl_counts, cast=check_lane_count,
            item_name="repetition count", src_loc_at=1 + src_loc_at)

        layout = SimdLayout(lambda lane_size:
                            self.input.layout.lane_bit_width(lane_size)
                            * self.lane_repl_counts[lane_size],
                            part_mode,
                            src_loc_at=1 + src_loc_at)
        super().__init__(layout, SimdPaddingKind.Repeating,
                         part_mode=part_mode, src_loc_at=1 + src_loc_at)

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from super().needed_submodules()
        yield from self.input.needed_submodules()

    def _get_lane_value(self, lane: SimdLane) -> _ValueCastableType:
        padded_width = self.layout.lane_bit_width(lane, include_padding=True)
        input_width = self.input.layout.lane_bit_width(lane)
        if input_width == 0:
            return Const(0, padded_width)
        repl_count = (padded_width + input_width - 1) // input_width
        return nmigen.Repl(self.input.lane(lane), repl_count)[0:padded_width]

    def __repr__(self) -> str:
        return f"SimdRepl({self.input}, {self.lane_repl_counts})"


@final
class SimdBoolCast(SimdValueLanewise):
    def __init__(self, input: _SimdValueCastable, *,
                 part_mode: Optional[SimdPartMode] = None,
                 src_loc_at: int = 0):
        self.input = SimdValue.cast(input, part_mode=part_mode,
                                    src_loc_at=1 + src_loc_at)
        assert part_mode is None or part_mode == self.input.layout.part_mode
        part_mode = self.input.layout.part_mode
        super().__init__(unsigned(1), SimdPaddingKind.SignExtended, part_mode,
                         src_loc_at=1 + src_loc_at)

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from super().needed_submodules()
        yield from self.input.needed_submodules()

    def _get_lane_value(self, lane: SimdLane) -> _ValueCastableType:
        padded_width = self.layout.lane_bit_width(lane, include_padding=True)
        return nmigen.Repl(self.input.lane(lane).bool(), padded_width)

    def __repr__(self) -> str:
        return f"SimdBoolCast({self.input})"


@final
class SimdSignal(SimdValue):
    def __init__(self, layout: _SimdLayoutCastable, *,
                 padding_kind: SimdPaddingKind = SimdPaddingKind.Other,
                 part_mode: Optional[SimdPartMode] = None,
                 name: Optional[str] = None,
                 reset_fill_bit: bool = False,
                 reset_less: bool = False,
                 attrs: Union[Iterable[Tuple[Any, Any]],
                              Mapping[Any, Any]] = (),
                 src_loc_at=0):
        layout = SimdLayout.cast(layout, part_mode,
                                 src_loc_at=1 + src_loc_at)
        reset = 0
        if reset_fill_bit:
            reset = (1 << layout.width) - 1
        underlying = Signal(shape=layout.width,
                            name=name, reset=reset, reset_less=reset_less,
                            attrs=attrs, decoder=None,
                            src_loc_at=src_loc_at + 1)
        super().__init__(layout, padding_kind, underlying)

    def eq(self, assigned_value: _SimdValueCastable, *,
           src_loc_at: int = 0) -> "SimdAssign":
        return SimdAssign(self, assigned_value, src_loc_at=1 + src_loc_at)

    def __repr__(self) -> str:
        return (f"SimdSignal(layout={self.layout}, "
                f"underlying={self.underlying})")


@final
class SimdAssign(SimdSimpleStatement):
    def __init__(self, lhs: SimdSignal, rhs: _SimdValueCastable, *,
                 src_loc_at: int = 0):
        assert isinstance(lhs, SimdSignal), "assigning to part selects, "\
            "slices, etc. of `SimdSignal`s is not supported"
        rhs = SimdValue.cast(rhs, lhs.layout.part_mode,
                             src_loc_at=1 + src_loc_at)
        rhs = rhs.convert_layout(lhs.layout, src_loc_at=1 + src_loc_at)
        self.lhs = lhs
        self.rhs = rhs

    def __repr__(self) -> str:
        return f"SimdAssign(lhs={self.lhs}, rhs={self.rhs})"

    def needed_submodules(self) -> Iterable[Elaboratable]:
        yield from self.lhs.needed_submodules()
        yield from self.rhs.needed_submodules()

    def _check_parent_context(self, parent_context: _StatementContext) -> None:
        return super()._check_parent_context(parent_context)
