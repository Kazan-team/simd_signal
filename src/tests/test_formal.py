# SPDX-License-Identifier: LGPL-3-or-later
# See Notices.txt for copyright information
from nmigen.hdl.ast import AnyConst, Assert, Assume, Signal
from .formal import formal
import unittest
from nmigen.hdl.dsl import Module


class TestFormal(unittest.TestCase):
    def test_formal1(self):
        m = Module()
        a = Signal(3)
        b = AnyConst(3)
        m.d.comb += a.eq(b)
        m.d.comb += Assume(b == 5)
        m.d.comb += Assert(a == 5)
        formal(self, m)
        # test running formal twice in the same test,
        # it should make different directories
        formal(self, m)

    def test_formal2(self):
        m = Module()
        a = Signal(10)
        b = Signal(10)
        c = Signal(20)
        m.d.comb += a.eq(AnyConst(10))
        m.d.comb += b.eq(AnyConst(10))
        m.d.comb += c.eq(a * b)
        m.d.comb += Assume(c == 1021 * 1019)  # two primes
        m.d.comb += Assert((a == 1021) | (a == 1019))
        formal(self, m)

    @unittest.expectedFailure
    def test_formal_failing(self):
        m = Module()
        a = Signal(10)
        b = Signal(10)
        c = Signal(20)
        m.d.comb += a.eq(AnyConst(10))
        m.d.comb += b.eq(AnyConst(10))
        m.d.comb += c.eq(a * b)
        m.d.comb += Assume(c == 1021 * 1019)  # two primes
        m.d.comb += Assert(a == 1021)  # it could equal 1019 instead
        formal(self, m)
