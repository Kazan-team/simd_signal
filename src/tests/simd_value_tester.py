# SPDX-License-Identifier: LGPL-3-or-later
# See Notices.txt for copyright information
from typing import Any, Callable, Generator, Iterable, List, Optional, Tuple
import nmigen
from nmigen.hdl.ast import AnyConst, Assert, Assume, Signal, Value, Const, signed, unsigned
from nmigen.hdl.dsl import Module
from nmigen.sim import Simulator, Delay
from simd_signal import (_SimdLayoutCastable, _SimdValueCastable,
                         SimdLane, SimdLayout, SimdPaddingKind, SimdPartMode, SimdSignal, SimdValue,
                         _ValueCastableType)
import unittest
from hashlib import sha256
from .util import get_test_path, _StrPath
from .formal import formal


_SimdValueTestable = Callable[[Tuple[SimdValue, ...]],
                              _SimdValueCastable]

_SimdValueTestReference = Callable[[SimdLane, Tuple[Value, ...]],
                                   _ValueCastableType]

_SimdValueTestCasePartMode = Tuple[bool, ...]
_SimdValueTestCaseInputs = Tuple[int, ...]
_SimdValueTestCase = Tuple[_SimdValueTestCasePartMode,
                           _SimdValueTestCaseInputs]


class SimdValueTester:
    layouts: List[SimdLayout]
    inputs: List[SimdSignal]

    def __init__(self,
                 operation: _SimdValueTestable,
                 reference: _SimdValueTestReference,
                 *layouts: _SimdLayoutCastable,
                 part_mode: Optional[SimdPartMode] = None,
                 src_loc_at: int = 0,
                 additional_case_count: int = 30,
                 special_cases: Iterable[_SimdValueTestCase] = (),
                 seed: str = "",
                 reference_includes_padding: bool = False):
        self.operation = operation
        self.reference = reference
        self.layouts = []
        self.inputs = []
        for layout in layouts:
            layout = SimdLayout.cast(layout, part_mode,
                                     src_loc_at=1 + src_loc_at)
            assert layout.part_mode == part_mode or part_mode is None
            part_mode = layout.part_mode
            self.layouts.append(layout)
            self.inputs.append(SimdSignal(layout,
                                          name=f"input_{len(self.inputs)}"))
        self.part_mode = SimdPartMode.get(part_mode)
        self.special_cases = list(special_cases)
        self.case_count = additional_case_count + len(self.special_cases)
        self.seed = seed
        self.reference_includes_padding = reference_includes_padding
        self.case_number = Signal(64)
        self.test_output_value = SimdValue.cast(operation(tuple(self.inputs)),
                                                part_mode=self.part_mode)
        self.reference_output_values = {
            lane: Value.cast(reference(lane, tuple(inp.lane(lane)
                                                   for inp in self.inputs)))
            for lane in self.part_mode.lanes()
        }
        self.test_output = SimdSignal(
            self.test_output_value.layout,
            padding_kind=self.test_output_value.padding_kind)
        self.reference_outputs = {
            lane: Signal(value.shape(),
                         name=f"reference_output_{lane.start}_{lane.size}")
            for lane, value in self.reference_output_values.items()
        }

    def __hash_256(self, v: str) -> int:
        return int.from_bytes(
            sha256(bytes(self.seed + v, encoding='utf-8')).digest(),
            byteorder='little'
        )

    def __hash(self, v: str, bits: int) -> int:
        retval = 0
        for i in range(0, bits, 256):
            retval <<= 256
            retval |= self.__hash_256(f" {v} {i}")
        return retval & ((1 << bits) - 1)

    def __get_case(self, case_number: int) -> _SimdValueTestCase:
        if case_number < len(self.special_cases):
            return self.special_cases[case_number]
        trial = 0
        while True:
            bits = self.__hash(f"{case_number} trial {trial}",
                               self.part_mode.part_count - 1)
            bits = 1 | (1 << self.part_mode.part_count) | (bits << 1)
            part_starts = tuple((bits & (1 << i)) != 0
                                for i in range(self.part_mode.part_count + 1))
            if SimdPartMode.are_part_starts_valid(part_starts, self.part_mode):
                break
            trial += 1
            if trial > 10000:
                raise ValueError(
                    f"can't generate valid SimdPartMode for "
                    f"case_number={case_number} seed={self.seed!r} "
                    f"log2_part_count={self.part_mode.log2_part_count} "
                    f"latest part_starts={part_starts!r}")
        inputs = tuple(self.__hash(f"{case_number} input {i}",
                                   self.layouts[i].width)
                       for i in range(len(self.layouts)))
        return part_starts, inputs

    def __format_case(self, case: _SimdValueTestCase) -> str:
        part_starts, inputs = case
        str_inputs = [hex(i) for i in inputs]
        return f"part_starts={part_starts}, inputs={str_inputs}"

    def __setup_case(self, case_number: int,
                     case: Optional[_SimdValueTestCase] = None
                     ) -> Generator[Any, int, None]:
        if case is None:
            case = self.__get_case(case_number)
        yield self.case_number.eq(case_number)
        part_starts, inputs = case
        for i in range(1, self.part_mode.part_count):
            yield self.part_mode.part_starts[i].eq(part_starts[i])
        for i in range(len(self.inputs)):
            s = self.inputs[i].underlying
            yield s.eq(inputs[i])

    def run_sim(self, test_case: unittest.TestCase, *,
                engine: Optional[str] = None,
                base_path: _StrPath = "sim_test_out"):
        m = Module()
        m.d.comb += self.test_output.underlying.eq(
            self.test_output_value.underlying)
        for i in self.test_output_value.needed_submodules():
            m.submodules += i
        for lane, value in self.reference_output_values.items():
            m.d.comb += self.reference_outputs[lane].eq(value)
        if engine is None:
            sim = Simulator(m)
        else:
            sim = Simulator(m, engine=engine)

        def check_active_lane(lane: SimdLane):
            reference = yield self.reference_outputs[lane]
            output = yield self.test_output.lane(
                lane, include_padding=self.reference_includes_padding)
            test_case.assertEqual(hex(reference), hex(output))
            unpadded_output = yield self.test_output.lane(lane)
            padded_output = yield self.test_output.lane(lane,
                                                        include_padding=True)
            padding_kind = self.test_output.padding_kind
            layout = self.test_output.layout
            padded_width = layout.lane_bit_width(lane, include_padding=True)
            unpadded_width = layout.lane_bit_width(lane)
            padding_width = padded_width - unpadded_width
            if padding_kind is SimdPaddingKind.SignExtended:
                sign_extended_output = \
                    Const.normalize(Const.normalize(unpadded_output,
                                                    signed(padded_width)),
                                    unsigned(padded_width))
                test_case.assertEqual(
                    hex(padded_output), hex(sign_extended_output),
                    "SimdPaddingKind.SignExtended requires padding be the "
                    "sign-extension of the corresponding lane")
            elif padding_kind is SimdPaddingKind.Zeros:
                expected_output = Const.normalize(unpadded_output,
                                                  unsigned(padded_width))
                test_case.assertEqual(
                    hex(padded_output), hex(expected_output),
                    "SimdPaddingKind.Zeros requires padding bits to be zero")
            elif padding_kind is SimdPaddingKind.Ones:
                expected_output = \
                    Const.normalize(~Const.normalize(~unpadded_output,
                                                     unsigned(unpadded_width)),
                                    unsigned(padded_width))
                test_case.assertEqual(
                    hex(padded_output), hex(expected_output),
                    "SimdPaddingKind.Ones requires padding bits to be all "
                    "ones")
            elif padding_kind is SimdPaddingKind.Other:
                pass
            elif padding_kind is SimdPaddingKind.Repeating:
                unpadded_output = Const.normalize(unpadded_output,
                                                  unsigned(unpadded_width))
                repl = 0
                for i in range(0, padded_width, unpadded_width):
                    repl |= unpadded_output << i
                repl_output = Const.normalize(repl, unsigned(padded_width))
                test_case.assertEqual(
                    hex(padded_output), hex(repl_output),
                    "SimdPaddingKind.Repeating requires padding bits to a "
                    "repeating copy of the lane's bits")
            else:
                assert padding_kind is SimdPaddingKind.NoPadding
                test_case.assertEqual(padding_width, 0,
                                      "SimdPaddingKind.NoPadding requires "
                                      "there be no padding")

        def check_case(case: _SimdValueTestCase):
            part_starts, inputs = case
            test_case.assertTrue((yield self.part_mode.is_valid()))
            for lane in self.part_mode.lanes():
                with test_case.subTest(lane=lane):
                    active = lane.is_active(part_starts)
                    expected_active = yield self.part_mode.is_lane_active(lane)
                    test_case.assertEqual(active, expected_active)
                    if active:
                        yield from check_active_lane(lane)

        def process():
            for case_number in range(self.case_count):
                with test_case.subTest(case_number=str(case_number)):
                    case = self.__get_case(case_number)
                    with test_case.subTest(case=self.__format_case(case)):
                        yield from self.__setup_case(case_number, case)
                        yield Delay(1e-6)
                        yield from check_case(case)
        sim.add_process(process)
        path = get_test_path(test_case, base_path)
        path.parent.mkdir(parents=True, exist_ok=True)
        vcd_path = path.with_suffix(".vcd")
        gtkw_path = path.with_suffix(".gtkw")
        traces = [self.case_number,
                  *self.part_mode.part_starts[1:-1]]
        for inp in self.inputs:
            traces.append(inp.underlying)
        traces.extend(self.reference_outputs.values())
        traces.append(self.test_output.underlying)
        with sim.write_vcd(vcd_path.open("wt", encoding="utf-8"),
                           gtkw_path.open("wt", encoding="utf-8"),
                           traces=traces):
            sim.run()

    def run_formal(self, test_case: unittest.TestCase, **kwargs):
        m = Module()
        m.d.comb += self.test_output.underlying.eq(
            self.test_output_value.underlying)
        for i in self.test_output_value.needed_submodules():
            m.submodules += i
        for lane, value in self.reference_output_values.items():
            m.d.comb += self.reference_outputs[lane].eq(value)
        m.d.comb += Assume(self.part_mode.is_valid())
        for i in range(1, self.part_mode.part_count):
            m.d.comb += self.part_mode.part_starts[i].eq(AnyConst(1))
        for i in range(len(self.inputs)):
            s = self.inputs[i].underlying
            m.d.comb += s.eq(AnyConst(s.shape()))

        def check_active_lane(lane: SimdLane) -> Iterable[Assert]:
            reference = self.reference_outputs[lane]
            output = self.test_output.lane(
                lane, include_padding=self.reference_includes_padding)
            yield Assert(reference == output)
            unpadded_output = self.test_output.lane(lane)
            padded_output = self.test_output.lane(lane,
                                                  include_padding=True)
            padding_kind = self.test_output.padding_kind
            layout = self.test_output.layout
            padded_width = layout.lane_bit_width(lane, include_padding=True)
            unpadded_width = layout.lane_bit_width(lane)
            padding_width = padded_width - unpadded_width
            expected_output = Signal(unsigned(padded_width),
                                     name=f"expected_output_"
                                     f"{lane.start}_{lane.size}")
            yield Assert(padded_output == expected_output)
            if padding_kind is SimdPaddingKind.SignExtended:
                m.d.comb += expected_output.eq(unpadded_output.as_signed())
            elif padding_kind is SimdPaddingKind.Zeros:
                m.d.comb += expected_output.eq(
                    unpadded_output[0:unpadded_width].as_unsigned())
            elif padding_kind is SimdPaddingKind.Ones:
                m.d.comb += expected_output.eq(nmigen.Cat(
                    unpadded_output[0:unpadded_width],
                    Const(-1, unsigned(padding_width))))
            elif padding_kind is SimdPaddingKind.Other:
                m.d.comb += expected_output.eq(padded_output)
            elif padding_kind is SimdPaddingKind.Repeating:
                m.d.comb += expected_output.eq(nmigen.Repl(
                    unpadded_output[0:unpadded_width],
                    len(range(0, padded_width, unpadded_width))))
            else:
                assert padding_kind is SimdPaddingKind.NoPadding
                test_case.assertEqual(padding_width, 0,
                                      "SimdPaddingKind.NoPadding requires "
                                      "there be no padding")
                m.d.comb += expected_output.eq(unpadded_output)

        for lane in self.part_mode.lanes():
            with test_case.subTest(lane=lane):
                asserts = [*check_active_lane(lane)]
                with m.If(self.part_mode.is_lane_active(lane)):
                    m.d.comb += asserts
        formal(test_case, m, **kwargs)
