# SPDX-License-Identifier: LGPL-3-or-later
# See Notices.txt for copyright information
from os import PathLike
from typing import Union
import unittest
from nmigen.hdl.ir import Elaboratable, Fragment
from nmigen.back import rtlil
import shutil
import textwrap
import subprocess
from .util import get_test_path, _StrPath

_FragmentLike = Union[Elaboratable, Fragment]


def formal(test_case: unittest.TestCase, hdl: _FragmentLike, *,
           base_path: _StrPath = "formal_test_temp"):
    hdl = Fragment.get(hdl, platform="formal")
    path = get_test_path(test_case, base_path)
    shutil.rmtree(path, ignore_errors=True)
    path.mkdir(parents=True)
    sby_name = "config.sby"
    sby_file = path / sby_name

    sby_file.write_text(textwrap.dedent(f"""\
    [options]
    mode prove
    depth 1
    wait on

    [engines]
    smtbmc

    [script]
    read_rtlil top.il
    prep

    [file top.il]
    {rtlil.convert(hdl)}
    """), encoding="utf-8")
    sby = shutil.which('sby')
    assert sby is not None
    with subprocess.Popen(
        [sby, sby_name],
        cwd=path, text=True, encoding="utf-8",
        stdin=subprocess.DEVNULL, stdout=subprocess.PIPE
    ) as p:
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            test_case.fail(f"Formal failed:\n{stdout}")
