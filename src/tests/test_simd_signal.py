# SPDX-License-Identifier: LGPL-3-or-later
# See Notices.txt for copyright information
from typing import Dict, Iterable, Optional, Set, Tuple, Union
import unittest
from nmigen.hdl.ast import Signal, signed, unsigned
import nmigen
from nmigen.hdl.dsl import Module
from nmigen.sim import Simulator, Delay
from simd_signal import (SimdCat, SimdLane, SimdPartMode, SimdLayout, SimdRepl,
                         UnusedLaneItemWarning)
from .simd_value_tester import SimdValueTester

Tuple_5_bool = Tuple[bool, bool, bool, bool, bool]

# same-length short aliases to make ACTIVE_LANES_4_PARTS readable
_1 = True
_0 = False

ACTIVE_LANES_4_PARTS: Dict[Tuple_5_bool, Optional[Set[SimdLane]]] = {
    (_1, _0, _0, _0, _1):  # [-----------4-part-------------]
    {SimdLane(0, 4, 2)},
    (_1, _1, _0, _0, _1):  # [1-part][XXX-----3-part-----XXX] 3-part isn't pow2
    None,
    (_1, _0, _1, _0, _1):  # [----2-part----][----2-part----]
    {SimdLane(0, 2, 2), SimdLane(2, 2, 2)},
    (_1, _1, _1, _0, _1):  # [1-part][1-part][----2-part----]
    {SimdLane(0, 1, 2), SimdLane(1, 1, 2), SimdLane(2, 2, 2)},
    (_1, _0, _0, _1, _1):  # [XXX-----3-part-----XXX][1-part] 3-part isn't pow2
    None,
    (_1, _1, _0, _1, _1):  # [1-part][XXX-2-part-XXX][1-part] 2-part unaligned
    None,
    (_1, _0, _1, _1, _1):  # [----2-part----][1-part][1-part]
    {SimdLane(0, 2, 2), SimdLane(2, 1, 2), SimdLane(3, 1, 2)},
    (_1, _1, _1, _1, _1):  # [1-part][1-part][1-part][1-part]
    {SimdLane(0, 1, 2), SimdLane(1, 1, 2),
     SimdLane(2, 1, 2), SimdLane(3, 1, 2)},
}


class TestSimdLane(unittest.TestCase):
    def test_constructor(self):
        lane = SimdLane(0, 2, 3)
        self.assertEqual(lane.log2_part_count, 3)
        self.assertEqual(lane.part_count, 8)
        self.assertEqual(lane.size, 2)
        self.assertEqual(lane.start, 0)
        self.assertEqual(repr(lane),
                         "SimdLane(start=0, size=2, "
                         "log2_part_count=3, part_count=8)")
        with self.assertRaises(AssertionError):
            SimdLane(1, 2, 3)
        with self.assertRaises(AssertionError):
            SimdLane(1, 0, 3)
        with self.assertRaises(AssertionError):
            SimdLane(0, 3, 3)
        with self.assertRaises(AssertionError):
            SimdLane(-1, 2, 3)
        with self.assertRaises(AssertionError):
            SimdLane(8, 2, 3)
        with self.assertRaises(AssertionError):
            SimdLane(4, 2, 2)
        lane = SimdLane(6, 2, 3)
        self.assertEqual(lane.log2_part_count, 3)
        self.assertEqual(lane.part_count, 8)
        self.assertEqual(lane.size, 2)
        self.assertEqual(lane.start, 6)
        self.assertEqual(repr(lane),
                         "SimdLane(start=6, size=2, "
                         "log2_part_count=3, part_count=8)")

    def test_eq_hash(self):
        self.assertEqual(SimdLane(2, 1, 3), SimdLane(2, 1, 3))
        self.assertNotEqual(SimdLane(0, 1, 3), SimdLane(2, 1, 3))
        self.assertNotEqual(SimdLane(2, 2, 3), SimdLane(2, 1, 3))
        self.assertNotEqual(SimdLane(2, 1, 2), SimdLane(2, 1, 3))
        self.assertEqual(hash(SimdLane(2, 1, 3)), hash(SimdLane(2, 1, 3)))
        self.assertEqual(hash(SimdLane(0, 2, 3)), hash(SimdLane(0, 2, 3)))

    def test_for_part(self):
        with self.assertRaises(AssertionError):
            SimdLane.for_part(0, 0, 3)
        lane = SimdLane.for_part(0, 2, 3)
        self.assertEqual(repr(lane),
                         "SimdLane(start=0, size=2, "
                         "log2_part_count=3, part_count=8)")
        lane = SimdLane.for_part(1, 2, 3)
        self.assertEqual(repr(lane),
                         "SimdLane(start=0, size=2, "
                         "log2_part_count=3, part_count=8)")
        lane = SimdLane.for_part(2, 2, 3)
        self.assertEqual(repr(lane),
                         "SimdLane(start=2, size=2, "
                         "log2_part_count=3, part_count=8)")
        lane = SimdLane.for_part(7, 2, 3)
        self.assertEqual(repr(lane),
                         "SimdLane(start=6, size=2, "
                         "log2_part_count=3, part_count=8)")
        lane = SimdLane.for_part(7, 1, 3)
        self.assertEqual(repr(lane),
                         "SimdLane(start=7, size=1, "
                         "log2_part_count=3, part_count=8)")
        lane = SimdLane.for_part(7, 4, 3)
        self.assertEqual(repr(lane),
                         "SimdLane(start=4, size=4, "
                         "log2_part_count=3, part_count=8)")
        lane = SimdLane.for_part(7, 8, 3)
        self.assertEqual(repr(lane),
                         "SimdLane(start=0, size=8, "
                         "log2_part_count=3, part_count=8)")

    def test_is_active(self):
        log2_part_count = 2
        part_count = 1 << log2_part_count

        def subtest(key: Tuple_5_bool, lane: SimdLane):
            active_lanes = ACTIVE_LANES_4_PARTS[key]
            assert active_lanes is not None
            expected_is_active = lane in active_lanes
            is_active = lane.is_active(key)
            self.assertEqual(expected_is_active, is_active)
        for key in part_modes(log2_part_count):
            for lane in SimdLane.lanes(log2_part_count):
                with self.subTest(key=key, lane=lane):
                    subtest(key, lane)


def part_modes(log2_part_count: Union[None, int, "SimdPartMode"] = None,
               only_valid: bool = True) -> Iterable[Tuple[bool, ...]]:
    log2_part_count = SimdPartMode.get_log2_part_count(log2_part_count)
    part_count = 1 << log2_part_count
    # all numbers in range that have lsb set and msb,
    # since both part_starts[0] and part_starts[part_count] are Const(1)
    bits_start = 1 | (1 << part_count)
    bits_end = bits_start + (1 << part_count)
    for bits in range(bits_start, bits_end, 2):
        # get key as tuple
        key = tuple((bits & (1 << bit_index)) != 0
                    for bit_index in range(part_count + 1))
        if only_valid:
            if not SimdPartMode.are_part_starts_valid(key, log2_part_count):
                continue
        yield key


def setup_part_mode(key: Tuple[bool, ...], part_mode: Optional[SimdPartMode] = None):
    part_mode = SimdPartMode.get(part_mode)
    for i in range(1, part_mode.part_count):
        yield part_mode.part_starts[i].eq(key[i])


def for_all_part_modes(test_case: unittest.TestCase,
                       part_mode: Optional[SimdPartMode] = None,
                       only_valid: bool = True):
    part_mode = SimdPartMode.get(part_mode)

    def decorator(f):
        def process():
            for key in part_modes(log2_part_count=part_mode,
                                  only_valid=only_valid):
                with test_case.subTest(key=key):
                    yield from setup_part_mode(key, part_mode)
                    yield from f(key)
        return process
    return decorator


class TestSimdPartMode(unittest.TestCase):
    def test_are_part_starts_valid(self):
        for key in part_modes(log2_part_count=2, only_valid=False):
            with self.subTest(key=key):
                expected_is_valid = ACTIVE_LANES_4_PARTS[key] is not None
                is_valid = SimdPartMode.are_part_starts_valid(
                    key, log2_part_count=2)
                self.assertEqual(expected_is_valid, is_valid)

    def test_4_part_is_valid(self):
        m = Module()
        part_mode_in = SimdPartMode(log2_part_count=2)  # 2**2 == 4 parts
        part_mode = SimdPartMode(log2_part_count=part_mode_in)
        is_valid = Signal()
        m.d.comb += is_valid.eq(part_mode.is_valid())
        active_lanes: Dict[SimdLane, Signal] = {}
        for lane in part_mode.lanes():
            sig = Signal(name=f"active_lanes_{lane.start}_{lane.size}")
            m.d.comb += sig.eq(part_mode.is_lane_active(
                SimdLane(lane.start, lane.size, part_mode)))
            active_lanes[lane] = sig
        m.d.comb += part_mode.eq(part_mode_in)
        sim = Simulator(m)

        @for_all_part_modes(self, part_mode_in, only_valid=False)
        def process(key: Tuple_5_bool):
            yield Delay(1e-6)
            expected_active_lanes = ACTIVE_LANES_4_PARTS[key]
            is_valid_v = yield is_valid
            if expected_active_lanes is not None:
                self.assertTrue(is_valid_v)
                for lane, sig in active_lanes.items():
                    lane_active = yield sig
                    expected = lane in expected_active_lanes
                    with self.subTest(lane=lane):
                        self.assertEqual(lane_active, expected)
            else:
                self.assertFalse(is_valid_v)

        sim.add_process(process)
        with sim.write_vcd("TestSimdPartMode.test_4_part_is_valid.vcd"):
            sim.run()

    def test_lanes(self):
        lanes = list(SimdPartMode(2).lanes())
        self.assertEqual(lanes, [
            SimdLane(0, 1, 2), SimdLane(1, 1, 2),
            SimdLane(2, 1, 2), SimdLane(3, 1, 2),
            SimdLane(0, 2, 2), SimdLane(2, 2, 2),
            SimdLane(0, 4, 2)])

        lanes = list(SimdPartMode(3).lanes())
        self.assertEqual(lanes, [
            SimdLane(0, 1, 3), SimdLane(1, 1, 3),
            SimdLane(2, 1, 3), SimdLane(3, 1, 3),
            SimdLane(4, 1, 3), SimdLane(5, 1, 3),
            SimdLane(6, 1, 3), SimdLane(7, 1, 3),
            SimdLane(0, 2, 3), SimdLane(2, 2, 3),
            SimdLane(4, 2, 3), SimdLane(6, 2, 3),
            SimdLane(0, 4, 3), SimdLane(4, 4, 3),
            SimdLane(0, 8, 3)])

    def test_scope(self):
        part_mode2 = SimdPartMode(2)
        part_mode3 = SimdPartMode(3)
        self.assertIsNone(SimdPartMode.get_opt())
        with self.assertRaisesRegex(ValueError, "Not in a SimdPartMode.scope"):
            SimdPartMode.get()
        self.assertIs(SimdPartMode.get(part_mode2), part_mode2)
        self.assertIs(SimdPartMode.get_opt(part_mode2), part_mode2)
        self.assertIs(SimdPartMode.get(part_mode3), part_mode3)
        self.assertIs(SimdPartMode.get_opt(part_mode3), part_mode3)
        with SimdPartMode.scope(part_mode2):
            self.assertIs(SimdPartMode.get(), part_mode2)
            self.assertIs(SimdPartMode.get_opt(), part_mode2)
            with SimdPartMode.scope(part_mode3):
                self.assertIs(SimdPartMode.get(), part_mode3)
                self.assertIs(SimdPartMode.get_opt(), part_mode3)
            self.assertIs(SimdPartMode.get(), part_mode2)
            self.assertIs(SimdPartMode.get_opt(), part_mode2)
        self.assertIsNone(SimdPartMode.get_opt())
        with self.assertRaisesRegex(ValueError, "Not in a SimdPartMode.scope"):
            SimdPartMode.get()
        with SimdPartMode.scope(part_mode3):
            self.assertIs(SimdPartMode.get(), part_mode3)
            self.assertIs(SimdPartMode.get_opt(), part_mode3)
        self.assertIsNone(SimdPartMode.get_opt())
        with self.assertRaisesRegex(ValueError, "Not in a SimdPartMode.scope"):
            SimdPartMode.get()

    def test_simple(self):
        part_mode3 = SimdPartMode(3)
        self.assertEqual(part_mode3.log2_part_count, 3)
        self.assertEqual(repr(part_mode3.part_starts),
                         "[(const 1'd1), "
                         "(sig part_starts_1), "
                         "(sig part_starts_2), "
                         "(sig part_starts_3), "
                         "(sig part_starts_4), "
                         "(sig part_starts_5), "
                         "(sig part_starts_6), "
                         "(sig part_starts_7), "
                         "(const 1'd1)]")
        self.assertEqual(repr(part_mode3),
                         "SimdPartMode(log2_part_count=3, part_count=8)")
        self.assertEqual(part_mode3.part_count, 8)
        self.assertEqual(tuple(part_mode3.lane_sizes()), (1, 2, 4, 8))
        part_mode2 = SimdPartMode(2)
        self.assertEqual(part_mode2.log2_part_count, 2)
        self.assertEqual(repr(part_mode2.part_starts),
                         "[(const 1'd1), "
                         "(sig part_starts_1), "
                         "(sig part_starts_2), "
                         "(sig part_starts_3), "
                         "(const 1'd1)]")
        self.assertEqual(repr(part_mode2),
                         "SimdPartMode(log2_part_count=2, part_count=4)")
        self.assertEqual(part_mode2.part_count, 4)
        self.assertEqual(tuple(part_mode2.lane_sizes()), (1, 2, 4))
        part_mode0 = SimdPartMode(0)
        self.assertEqual(part_mode0.log2_part_count, 0)
        self.assertEqual(repr(part_mode0.part_starts),
                         "[(const 1'd1), (const 1'd1)]")
        self.assertEqual(repr(part_mode0),
                         "SimdPartMode(log2_part_count=0, part_count=1)")
        self.assertEqual(part_mode0.part_count, 1)
        self.assertEqual(tuple(part_mode0.lane_sizes()), (1,))
        self.assertEqual(part_mode0, part_mode0)
        self.assertNotEqual(part_mode0, part_mode2)
        self.assertNotEqual(part_mode0, part_mode3)
        self.assertNotEqual(part_mode2, part_mode0)
        self.assertEqual(part_mode2, part_mode2)
        self.assertNotEqual(part_mode2, part_mode3)
        self.assertNotEqual(part_mode3, part_mode0)
        self.assertNotEqual(part_mode3, part_mode2)
        self.assertEqual(part_mode3, part_mode3)


class TestSimdLayout(unittest.TestCase):
    def test_1_bit(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            self.assertEqual(
                repr(SimdLayout(1)),
                "SimdLayout(lane_shapes={1: unsigned(1), 2: unsigned(1),"
                " 4: unsigned(1), 8: unsigned(1)}, padded_bits_per_part=1, "
                "width=8, lanes_signed=False, "
                "part_mode=SimdPartMode(log2_part_count=3, part_count=8))")

    def test_2_bit(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            self.assertEqual(
                repr(SimdLayout(2)),
                "SimdLayout(lane_shapes={1: unsigned(2), 2: unsigned(2),"
                " 4: unsigned(2), 8: unsigned(2)}, padded_bits_per_part=2, "
                "width=16, lanes_signed=False, "
                "part_mode=SimdPartMode(log2_part_count=3, part_count=8))")

    def test_2_bit_signed(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            self.assertEqual(
                repr(SimdLayout(signed(2))),
                "SimdLayout(lane_shapes={1: signed(2), 2: signed(2), "
                "4: signed(2), 8: signed(2)}, padded_bits_per_part=2, "
                "width=16, lanes_signed=True, "
                "part_mode=SimdPartMode(log2_part_count=3, part_count=8))")

    def test_3_4_5_6_bit(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            a = SimdLayout({1: 3, 2: 4, 4: 5, 8: 6})
            self.assertEqual(
                repr(a),
                "SimdLayout(lane_shapes={1: unsigned(3), 2: unsigned(4),"
                " 4: unsigned(5), 8: unsigned(6)}, padded_bits_per_part=3, "
                "width=24, lanes_signed=False, "
                "part_mode=SimdPartMode(log2_part_count=3, part_count=8))")
            self.assertEqual(
                repr(a.with_padding()),
                "SimdLayout(lane_shapes={1: unsigned(3), 2: unsigned(6),"
                " 4: unsigned(12), 8: unsigned(24)}, padded_bits_per_part=3, "
                "width=24, lanes_signed=False, "
                "part_mode=SimdPartMode(log2_part_count=3, part_count=8))")

    def test_8_16_32_64_bit(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            a = SimdLayout({1: 8, 2: 16, 4: 32, 8: 64})
            b = SimdLayout(lambda lane_size: 8 * lane_size)
            self.assertEqual(a, b)
            self.assertIs(
                a, a.with_padding(),
                msg="fully padded layouts' with_padding returns self")
            self.assertEqual(
                repr(a),
                "SimdLayout(lane_shapes={1: unsigned(8), "
                "2: unsigned(16), 4: unsigned(32), 8: unsigned(64)}, "
                "padded_bits_per_part=8, width=64, lanes_signed=False, "
                "part_mode=SimdPartMode(log2_part_count=3, part_count=8))")
            self.assertEqual(
                repr(b),
                "SimdLayout(lane_shapes={1: unsigned(8), "
                "2: unsigned(16), 4: unsigned(32), 8: unsigned(64)}, "
                "padded_bits_per_part=8, width=64, lanes_signed=False, "
                "part_mode=SimdPartMode(log2_part_count=3, part_count=8))")

    def test_eq(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            a = SimdLayout.cast({1: 8, 2: 16, 4: 32, 8: 64})
            b = SimdLayout(lambda lane_size: 8 * lane_size)
            self.assertEqual(a, a)
            self.assertEqual(a, SimdLayout(a))
            self.assertEqual(a, b)
            self.assertEqual(b, b)
            c = SimdLayout(lambda lane_size: lane_size)
            self.assertNotEqual(a, c)
            self.assertNotEqual(b, c)
            self.assertEqual(c, c)
        # check that SimdLayout.cast returns input SimdLayout unmodified
        # without needing SimdPartMode.scope
        self.assertIs(a, SimdLayout.cast(a))
        # make a new SimdPartMode
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            d = SimdLayout(lambda lane_size: lane_size)
            self.assertEqual(d, d)
            self.assertNotEqual(c, d)
            e = SimdLayout(lambda lane_size: 8 * lane_size)
            self.assertEqual(e, e)
            self.assertNotEqual(d, e)
            self.assertNotEqual(a, e)
        self.assertNotEqual(a, 0)
        self.assertNotEqual("blah", a)

    def test_unused_lane_shape_warning(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            with self.assertWarnsRegex(
                    UnusedLaneItemWarning,
                    r"SimdLayout: unused lane shape: \(3, 10\)"):
                SimdLayout({3: 10, 1: 1, 2: 2, 4: 4, 8: 8})

    def test_missing_lane_shape(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            with self.assertRaisesRegex(ValueError,
                                        r"missing lane shape for lane_size=1"):
                SimdLayout({})

    def test_bad_shape(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            with self.assertRaises(TypeError):
                SimdLayout(None)
            with self.assertRaises(TypeError):
                SimdLayout(lambda lane_size: None)

    def test_unequal_signedness(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=3)):
            with self.assertRaisesRegex(
                    ValueError, r"all lane shapes must have same signedness"):
                SimdLayout({1: unsigned(1), 2: signed(2),
                            4: unsigned(4), 8: unsigned(8)})

    def test_1_bit_bits_slice(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=2)):
            layout = SimdLayout(1)
            self.assertEqual(layout.lane_bits_slice(SimdLane(0, 4)),
                             slice(0, 1))
            self.assertEqual(layout.lane_bits_slice(SimdLane(0, 2)),
                             slice(0, 1))
            self.assertEqual(layout.lane_bits_slice(SimdLane(2, 2)),
                             slice(2, 3))
            self.assertEqual(layout.lane_bits_slice(SimdLane(0, 1)),
                             slice(0, 1))
            self.assertEqual(layout.lane_bits_slice(SimdLane(1, 1)),
                             slice(1, 2))
            self.assertEqual(layout.lane_bits_slice(SimdLane(2, 1)),
                             slice(2, 3))
            self.assertEqual(layout.lane_bits_slice(SimdLane(3, 1)),
                             slice(3, 4))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(0, 4), include_padding=True), slice(0, 4))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(0, 2), include_padding=True), slice(0, 2))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(2, 2), include_padding=True), slice(2, 4))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(0, 1), include_padding=True), slice(0, 1))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(1, 1), include_padding=True), slice(1, 2))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(2, 1), include_padding=True), slice(2, 3))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(3, 1), include_padding=True), slice(3, 4))
            self.assertEqual(layout.part_bits_slice(0), slice(0, 1))
            self.assertEqual(layout.part_bits_slice(1), slice(1, 2))
            self.assertEqual(layout.part_bits_slice(2), slice(2, 3))
            self.assertEqual(layout.part_bits_slice(3), slice(3, 4))

    def test_3_7_10_bit_bits_slice(self):
        with SimdPartMode.scope(SimdPartMode(log2_part_count=2)):
            layout = SimdLayout({1: 3, 2: 7, 4: 10})
            self.assertEqual(layout.padded_bits_per_part, 4)
            self.assertEqual(layout.lane_bits_slice(SimdLane(0, 4)),
                             slice(0, 10))
            self.assertEqual(layout.lane_bits_slice(SimdLane(0, 2)),
                             slice(0, 7))
            self.assertEqual(layout.lane_bits_slice(SimdLane(2, 2)),
                             slice(8, 15))
            self.assertEqual(layout.lane_bits_slice(SimdLane(0, 1)),
                             slice(0, 3))
            self.assertEqual(layout.lane_bits_slice(SimdLane(1, 1)),
                             slice(4, 7))
            self.assertEqual(layout.lane_bits_slice(SimdLane(2, 1)),
                             slice(8, 11))
            self.assertEqual(layout.lane_bits_slice(SimdLane(3, 1)),
                             slice(12, 15))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(0, 4), include_padding=True), slice(0, 16))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(0, 2), include_padding=True), slice(0, 8))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(2, 2), include_padding=True), slice(8, 16))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(0, 1), include_padding=True), slice(0, 4))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(1, 1), include_padding=True), slice(4, 8))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(2, 1), include_padding=True), slice(8, 12))
            self.assertEqual(layout.lane_bits_slice(
                SimdLane(3, 1), include_padding=True), slice(12, 16))
            self.assertEqual(layout.part_bits_slice(0), slice(0, 4))
            self.assertEqual(layout.part_bits_slice(1), slice(4, 8))
            self.assertEqual(layout.part_bits_slice(2), slice(8, 12))
            self.assertEqual(layout.part_bits_slice(3), slice(12, 16))


class TestSimdValue(unittest.TestCase):
    def test_identity(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: values[0],
                            lambda lane, values: values[0],
                            unsigned(4)).run_sim(self)

    def test_constructor(self):
        # m = Module()
        # TODO
        pass


class TestSimdCat(unittest.TestCase):
    def test_sim_empty(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdCat(*values),
                            lambda lane, values: nmigen.Cat(*values)
                            ).run_sim(self)

    def test_sim_u4(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdCat(*values),
                            lambda lane, values: nmigen.Cat(*values),
                            unsigned(4),
                            unsigned(4),
                            unsigned(4)).run_sim(self)

    def test_sim_u3__u5_u6_u7__s1(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdCat(*values),
                            lambda lane, values: nmigen.Cat(*values),
                            unsigned(3),
                            {1: unsigned(5), 2: unsigned(6), 4: unsigned(7)},
                            signed(1)).run_sim(self)

    def test_sim_u4_u8_u16_u32__u4_u8_u16_u32(self):
        with SimdPartMode.scope(SimdPartMode(3)):
            SimdValueTester(lambda values: SimdCat(*values),
                            lambda lane, values: nmigen.Cat(*values),
                            lambda lane_size: unsigned(4 * lane_size),
                            lambda lane_size: unsigned(4 * lane_size),
                            ).run_sim(self)

    def test_formal_u4(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdCat(*values),
                            lambda lane, values: nmigen.Cat(*values),
                            unsigned(4),
                            unsigned(4),
                            unsigned(4)).run_formal(self)

    def test_formal_u3__u5_u6_u7__s1(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdCat(*values),
                            lambda lane, values: nmigen.Cat(*values),
                            unsigned(3),
                            {1: unsigned(5), 2: unsigned(6), 4: unsigned(7)},
                            signed(1)).run_formal(self)

    def test_formal_u4_u8_u16_u32__u4_u8_u16_u32(self):
        with SimdPartMode.scope(SimdPartMode(3)):
            SimdValueTester(lambda values: SimdCat(*values),
                            lambda lane, values: nmigen.Cat(*values),
                            lambda lane_size: unsigned(4 * lane_size),
                            lambda lane_size: unsigned(4 * lane_size),
                            ).run_formal(self)


class TestSimdRepl(unittest.TestCase):
    def test_sim_u8_repl_0(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdRepl(values[0], 0),
                            lambda lane, values: nmigen.Repl(values[0], 0),
                            unsigned(8)).run_sim(self)

    def test_sim_u4_repl_3(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdRepl(values[0], 3),
                            lambda lane, values: nmigen.Repl(values[0], 3),
                            unsigned(4)).run_sim(self)

    def test_sim_u5_u6_u7_repl_1_2_3(self):
        repl_counts = {1: 1, 2: 2, 4: 3}
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdRepl(values[0], repl_counts),
                            lambda lane, values:
                            nmigen.Repl(values[0], repl_counts[lane.size]),
                            {1: unsigned(5), 2: unsigned(6), 4: unsigned(7)}
                            ).run_sim(self)

    def test_sim_u4_u8_u16_u32_repl_2(self):
        with SimdPartMode.scope(SimdPartMode(3)):
            SimdValueTester(lambda values: SimdRepl(values[0], 2),
                            lambda lane, values: nmigen.Repl(values[0], 2),
                            lambda lane_size: unsigned(4 * lane_size),
                            ).run_sim(self)

    def test_formal_u4_repl_3(self):
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdRepl(values[0], 3),
                            lambda lane, values: nmigen.Repl(values[0], 3),
                            unsigned(4)).run_formal(self)

    def test_formal_u5_u6_u7_repl_1_2_3(self):
        repl_counts = {1: 1, 2: 2, 4: 3}
        with SimdPartMode.scope(SimdPartMode(2)):
            SimdValueTester(lambda values: SimdRepl(values[0], repl_counts),
                            lambda lane, values:
                            nmigen.Repl(values[0], repl_counts[lane.size]),
                            {1: unsigned(5), 2: unsigned(6), 4: unsigned(7)}
                            ).run_formal(self)

    def test_formal_u4_u8_u16_u32_repl_2(self):
        with SimdPartMode.scope(SimdPartMode(3)):
            SimdValueTester(lambda values: SimdRepl(values[0], 2),
                            lambda lane, values: nmigen.Repl(values[0], 2),
                            lambda lane_size: unsigned(4 * lane_size),
                            ).run_formal(self)


if __name__ == "__main__":
    unittest.main()
